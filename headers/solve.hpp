#pragma once
#include <cmath>

const double eps = 0.00000000001;

double* solve(double a, double b, double c, double eps_ = eps);