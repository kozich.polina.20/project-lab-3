#include <gtest/gtest.h>
#include "solve.hpp"
#include <cmath>

const double eps_test = 0.00000000001;

bool eq_double(double a, double b, double eps_ = eps_test){
    return abs(a - b) < eps_; 
}

bool expected_two(double* answer, double x1, double x2){
    return eq_double(answer[0], x1) && eq_double(answer[1], x2) 
        || eq_double(answer[1], x1) && eq_double(answer[0], x2);
}

TEST(quad_equation_test, one_minus_two_one)
{
    double* answer = solve(1, -2, 1);
    EXPECT_TRUE(eq_double(answer[0], 1.0));
    EXPECT_TRUE(eq_double(answer[1], 1.0));
}

TEST(quad_equation_test, one_zero_minus_one)
{
    double* answer = solve(1, 0, -1);
    EXPECT_TRUE(expected_two(answer, -1.0, 1.0));
}

TEST(quad_equation_test, one_zero_one)
{
    double* answer = solve(1, 0, 1);
    ASSERT_EQ(answer, nullptr);
}

TEST(quad_equation_test, zero_a)
{
    double* answer = solve(0, 123131312, 12313213212);
    ASSERT_EQ(answer, nullptr);
}

