#include "solve.hpp"

double* solve(double a, double b, double c, double eps_) {
    double d = b*b - 4*a*c;
    if (d < -eps_) return nullptr;
    if (abs(a) < eps_) return nullptr; 
    double* result = new double[2];
    result[0] = (-b - sqrt(d)) / (2.0 * a);
    result[1] = (-b + sqrt(d)) / (2.0 * a);
    return result;
}

